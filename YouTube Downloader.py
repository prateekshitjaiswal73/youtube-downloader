# '''

# Made By - Prateekshit JAiswal

# '''


from pytube import YouTube

from playsound import playsound

import tkinter as tk

from tkinter.ttk import Progressbar

from tkinter import ttk

import time

# from PIL import ImageTk, Image


WINDOW_WIDTH = 500

WINDOW_HEIGHT = 500

WINDOW_TITLE = "Youtube downloader"

BUTTON_CLICK_SOUND = "clicks.m4a"


class YoutubeDownloader:

     '''

     Class to create an app that downloads youtube videos

     '''

     def __init__(self):
         self.window = tk.Tk(

        )

        self.window.geometry(
            "{}x{}".format(WINDOW_WIDTH, WINDOW_HEIGHT)
        )

        self.window.resizable(
            width=True, height=True
        )

        self.window.configure(
            bg="#712093"
        )

        self.window.title(
            WINDOW_TITLE
        )

        # img = ImageTk.PhotoImage(Image.open("Banner.jpg"))

        self.img_label = tk.Label(
            self.window, text="YouTube Video Downloader", background="yellow", width=30
        )

        self.img_label.place(
            x=130, y=10
        )

        self.link_label = tk.Label(
            self.window, text="Download Link", width="13"
        )

        self.link_label.place(
            x=3, y=120
        )

        self.name_label = tk.Label(
            self.window, text="Save File as", width="13"
        )

        self.name_label.place(
            x=3, y=160
        )

        self.path_label = tk.Label(
            self.window, text="Save File Path", width="13"
        )

        self.path_label.place(
            x=3, y=200
        )

        self.ext_label = tk.Label(
            self.window, text="File extension", width="13"
        )

        self.ext_label.place(
            x=3, y=240
        )

        self.link_entry = tk.Entry(
            master=self.window, width=40

        )

        self.link_entry.place(
            x=150, y=120
        )

        self.name_entry = tk.Entry(
            master=self.window, width=40,

        )

        self.name_entry.place(
            x=150, y=160
        )

        self.path_entry = tk.Entry(
            master=self.window, width=40,

        )

        self.path_entry.place(
            x=150, y=200
        )

        self.ext_entry = tk.Entry(
            master=self.window, width=40,

        )

        self.ext_entry.place(
            x=150, y=240
        )

        self.download_button = tk.Button(
            self.window, text="Download",

            command=lambda: [self.__bar(), self.__get_link()]
        )

        self.download_button.place(
            x=205, y=450
        )

        self.path_label = tk.Label(
            self.window, text="By ~ Prateekshit Jaiswal", width="25",

            cursor="dot", background="lightgreen", font="FiraCodeMedium 10"
        )

        self.path_label.place(
            x=300, y=35
        )

        #    grid(column=3, row=120)

        return

    def __bar(self):
        playsound(
            BUTTON_CLICK_SOUND
        )

        style = ttk.Style(

        )

        style.theme_use(
            'default'
        )

        style.configure(
            "grey.Horizontal.TProgressbar", background='blue'
        )

        bar = Progressbar(
            self.window, length=280, style='grey.Horizontal.TProgressbar', mode='determinate'
        )

        bar.place(
            x=120, y=400
        )

        bar['value'] = 20

        self.window.update_idletasks(

        )

        time.sleep(
            1
        )

        bar['value'] = 40

        self.window.update_idletasks(

        )

        time.sleep(
            1
        )

        bar['value'] = 50

        self.window.update_idletasks(

        )

        time.sleep(
            1
        )

        bar['value'] = 60

        self.window.update_idletasks(

        )

        time.sleep(
            1
        )

        bar['value'] = 80

        self.window.update_idletasks(

        )

        time.sleep(
            1
        )

        bar['value'] = 100

    def __downloader(self, link, save_path="", save_name="", extension="mp4"):
        yt = YouTube(
            link
        )

        yt_stream = yt.streams.filter(
            progressive=True, file_extension=extension
        ).order_by(
            'resolution'
        ).desc(

        ).first(

        )

        yt_stream.download(
            output_path=save_path, filename=save_name
        )

        return

    def __get_link(self):
        link = self.link_entry.get(

        )

        path = self.path_entry.get(

        )

        name = self.name_entry.get(

        )

        ext = self.ext_entry.get(

        )

        self.__downloader(
            link, path, name, ext
        )

        return

    def run_app(self):
        self.window.mainloop(

        )

        return


if __name__ == "__main__":
    app = YoutubeDownloader(

    )

    app.run_app(

    )
